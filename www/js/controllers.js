angular.module('starter.controllers', ['btford.socket-io'])


.factory('socket',function(socketFactory){
  //Create socket and connect to http://chat.socket.io 
  var myIoSocket = io.connect('http://10.0.1.228:9001');

    mySocket = socketFactory({
        ioSocket: myIoSocket
    });
    
  return mySocket;
  
})

.controller('AppCtrl', function($scope, $ionicModal, $timeout) {

  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //$scope.$on('$ionicView.enter', function(e) {
  //});

  // Form data for the login modal
  $scope.loginData = {};

  // Create the login modal that we will use later
  $ionicModal.fromTemplateUrl('templates/login.html', {
    scope: $scope
  }).then(function(modal) {
    $scope.modal = modal;
  });

  // Triggered in the login modal to close it
  $scope.closeLogin = function() {
    $scope.modal.hide();
  };

  // Open the login modal
  $scope.login = function() {
    $scope.modal.show();
  };

  // Perform the login action when the user submits the login form
  $scope.doLogin = function() {
    console.log('Doing login', $scope.loginData);

    // Simulate a login delay. Remove this and replace with your login
    // code if using a login system
    $timeout(function() {
      $scope.closeLogin();
    }, 1000);
  };
})

.controller('PlaylistsCtrl', function($scope,$cordovaSQLite,$rootScope,$ionicLoading) {


  $scope.playlists = [
      { title: 'Reggae', id: 1 },
      { title: 'Chill', id: 2 },
      { title: 'Dubstep', id: 3 },
      { title: 'Indie', id: 4 },
      { title: 'Rap', id: 5 },
      { title: 'Cowbell', id: 6 }
  ];


})

.controller('PlaylistCtrl', function($scope, $stateParams) {
})

.controller('InventarioCtrl', function($scope,$cordovaSQLite, $rootScope,$ionicLoading,$q,$ionicPopup,$ionicPlatform,socket){

    $scope.sincronizados_init  = 0;
    $scope.sincronizados_fin   = 0;
    $scope.show_hide_sinc_info = 'display:none';

    $scope.glosa               = '';
    $scope.cantidad            = '';
    $scope.idproducto          = '';
    $scope.idean               = '';
    $scope.iddun               = '';
    $scope.fecha               = '';


    $ionicLoading.show({
        template   : 'Cargando...',
        noBackdrop : true
    });


    var myVar = setInterval(function(){

        /*var db    = $rootScope.db;
        $cordovaSQLite.execute(db, "CREATE TABLE IF NOT EXISTS people (id integer primary key, firstname text, lastname text)");
        var query = "INSERT INTO people (firstname, lastname) VALUES (?,?)";
        $cordovaSQLite.execute(db, query, ['Patricio', 'Betancourt']).then(function(res) {
            alert("INSERT ID -> " + res.insertId);
        }, function (err) { 
            alert(err);
        });
        var query2 = "SELECT firstname, lastname FROM people WHERE lastname = ?";
        $cordovaSQLite.execute(db, query2, ['Betancourt']).then(function(res) {
            if(res.rows.length > 0) {
                alert("SELECTED -> " + res.rows.item(0).firstname + " " + res.rows.item(0).lastname);
            } else {
                alert("No results found");
            }
        }, function (err) {
            alert(err);
        });  */

        if($rootScope.flag){
            $scope.init();
            $rootScope.flag = false;
        }

        $scope.myStopFunction();  


    }, 5000);


    $scope.myStopFunction = function() {
        clearInterval(myVar);
        $ionicLoading.hide();
    }

    $scope.init = function(){

        $cordovaSQLite.execute($rootScope.db, "CREATE TABLE IF NOT EXISTS DIM_PRODUCTOS (idproducto text, idean text, iddun text, glosa text)");
        $scope.select_all_productos();
    }


    $scope.sincronizar = function() {

        socket.emit('sincronizar');  


        socket.on('sincronizar', function(data){

            $scope.sincronizados_init = 0;
            $scope.sincronizados_fin  = 0;

            //alert('Preparando insert productos...');
            //alert('Largo de la data: ' +data.length);

            $scope.insert_productos(data);

        });
    }


    $scope.insert_productos = function(data){

        //alert('Entró insert productos....');

        $scope.show_hide_sinc_info = 'display:block';
        $scope.sincronizados_fin   = data.length;

        var q                      = $q.defer();
        q.notify();


        //alert('Declaró defer....');

        $scope.resp_dele_prod  = $scope.delete_productos();

        if($scope.resp_dele_prod){

            for(var i = 0; i < data.length; i++){

                try {

                    var query = "INSERT INTO DIM_PRODUCTOS (idproducto, idean, iddun, glosa) VALUES (?,?,?,?)";
                    $cordovaSQLite.execute($rootScope.db, query, [ data[i].idproducto , data[i].idean , data[i].iddun, data[i].glosa]).then(function(res) { 

                        //alert('INSERT DIM_PRODUCTOS ID -> '+ res.insertId); 

                        q.resolve($scope.sincronizados_init++);
                        q.resolve($scope.validar_fin_sincronizacion());
                        

                    }, function (err) {

                        alert(err);  
                        console.error(err);
                        q.reject(err);
                        //$ionicLoading.hide();

                    });
                }
                catch (e) {
                    alert(e);    //Error
                }
            }

            return q.promise;
            
        }

    }


    $scope.delete_productos = function(){

        var q     = $q.defer();
        q.notify();

        try {
            var query = "DELETE FROM DIM_PRODUCTOS";
            $cordovaSQLite.execute($rootScope.db, query).then(function(result) {

                q.resolve(result);

            }, function (err) {

                alert(err);
                q.reject(err);

            });
        }
        catch (e) {
            alert(e);       //never executed
        }

        return q.promise;

    }


    $scope.validar_fin_sincronizacion = function(){


        if($scope.sincronizados_fin == $scope.sincronizados_init){

            $scope.select_all_productos();
            //$scope.showAlert_fin_sincronizacion();
            document.getElementById('codigo_barra').focus(); 

        }

    }


    $scope.select_porcodigo_productos = function(codigo){

      try {
          $cordovaSQLite.execute($rootScope.db, "SELECT * FROM DIM_PRODUCTOS WHERE idean='"+codigo+"' or iddun='"+codigo+"' or idproducto='"+codigo+"' ").then(function(res) { 

              $scope.glosa       = res.rows.item(0).glosa;
              $scope.idproducto  = res.rows.item(0).idproducto;
              $scope.idean       = res.rows.item(0).idean;
              $scope.iddun       = res.rows.item(0).iddun;

          }, function (err) {

              alert(err);

          });
      }
      catch (e) {
          alert(e);       //never executed
      }

    }

    $scope.select_all_productos = function(){

      try {
          var query = "SELECT * FROM DIM_PRODUCTOS";  
          $cordovaSQLite.execute($rootScope.db, query).then(function(res) {
         
          if(res.rows.length > 0) {

              /*for(var i = 0; i < res.rows.length; i++) {
                  alert(res.rows.item(i).glosa);  //res.rows.item(0).firstname
              }*/
          }
          else{
              $scope.show_sin_productos();
          }
              
  
          }, function (err) {
              alert(err);
          });
      }
      catch (e) {
          alert(e); //Error
      }

    }


    $scope.guardar_inventario = function(){
        alert('Guardar Inventario');
    }

    $scope.subir_erp = function(){ 
        alert('Subir ERP');
    }


    $scope.buscar_producto = function(codigo){
        $scope.select_porcodigo_productos(codigo);
    }


    $scope.show_sin_productos = function() {

      var alertPopup = $ionicPopup.alert({
          title    : 'Sin Productos',
          template : 'No se encontraron productos, por favor sincronizar.'
      });

      alertPopup.then(function(res) {
          console.log('Ionic PopUp Ejecutado');
        
      });

    };



})

.controller('CrearInventarioCtrl', function($scope,socket,$cordovaSQLite, $ionicLoading,$q,$ionicPopup,$ionicPlatform) {

  

});
