var db     = null;
var sqlite = angular.module('sqlite', ['ionic', 'ngCordova']);

sqlite.run(function($ionicPlatform, $cordovaSQLite) {
  	$ionicPlatform.ready(function() {

    	db  = $cordovaSQLite.openDB({ name: 'myapp.db', location: 'default' });
    	$cordovaSQLite.execute(db, "CREATE TABLE IF NOT EXISTS people (id integer primary key, firstname text, lastname text)");
  	
  	});
});


sqlite.factory('queries', function($cordovaSQLite) {
  return {
    insert : function(firstname, lastname, avatar, message) {

      	var query = "INSERT INTO people (firstname, lastname) VALUES (?,?)";
	    $cordovaSQLite.execute(db, query, ['Patricio', 'Betancourt']).then(function(res) {
	          alert("INSERT ID -> " + res.insertId);
	    }, function (err) {
	          alert(err);
	    });
    },
    select : function() {
    	var query2 = "SELECT firstname, lastname FROM people WHERE lastname = ?";
	    $cordovaSQLite.execute(db, query2, ['Betancourt']).then(function(res) {
	          if(res.rows.length > 0) {
	              alert("SELECTED -> " + res.rows.item(0).firstname + " " + res.rows.item(0).lastname);
	          } else {
	              alert("No results found");
	          }
	    }, function (err) {
	          alert(err);
	    });
    }
  }
});